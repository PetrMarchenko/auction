<?php


namespace Goods\Entity;


use Application\Entity\Entity;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;


/**
 * A list goods.
 *
 * @ORM\Entity
 * @ORM\Table(name="goods")
 * @property string $name
 * @property integer $price
 * @property integer $typeId
 * @property integer $startPrice
 * @property integer $stepPrice
 * @property string $description
 * @property string $photo
 * @property /DateTime $dateStart
 * @property /DateTime $dateStop
 * @property bool $dynamic
 * @property integer $buyer
 * @property int $id
 */
class Goods extends Entity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint", length=20);
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50);
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true);
     */
    protected $descriptions;

    /**
     * @ORM\Column(type="integer");
     */
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="TypeGoods")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    protected $typeId;

    /**
     * @ORM\Column(type="bigint", name="start_price");
     */
    protected $startPrice;

    /**
     * @ORM\Column(type="integer", name="step_price", nullable=true);
     */
    protected $stepPrice;

    /**
     * @ORM\Column(type="string", length=250, nullable=true);
     */
    protected $photo;

    /**
     * @ORM\Column(type="datetime", name="date_start")
     */
    protected $dateStart;

    /**
     * @ORM\Column(type="datetime", name="date_stop")
     */
    protected $dateStop;

    /**
     * @ORM\Column(type="bigint", length=20, nullable=true);
     */
    protected $buyer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $dynamic;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $descriptions
     * @return $this
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;
        return $this;
    }

    /**
     * @return int
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @param $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param $startPrice
     * @return $this
     */
    public function setStartPrice($startPrice)
    {
        $this->startPrice = $startPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getStartPrice()
    {
        return $this->startPrice;
    }

    /**
     * @param $stepPrice
     * @return $this
     */
    public function setStepPrice($stepPrice)
    {
        $this->stepPrice = $stepPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getStepPrice()
    {
        return $this->stepPrice;
    }

    /**
     * @param $photo
     * @return $this
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param $dateStart
     * @return $this
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param $dateStop
     * @return $this
     */
    public function setDateStop($dateStop)
    {
        $this->dateStop = $dateStop;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateStop()
    {
        return $this->dateStop;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
        return $this;
    }

    /**
     * @return TypeGoods
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param $buyer
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;
    }

    /**
     * @return int
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * @param $dynamic
     * @return $this
     */
    public function setDynamic($dynamic) {
        $this->dynamic = $dynamic;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDynamic()
    {
        return $this->dynamic;
    }

    /**
     * @return float
     */
    public function getPriceDecimal()
    {
        return $this->price / 100;
    }

    /**
     * @return float
     */
    public function getStartPriceDecimal()
    {
        return $this->startPrice / 100;
    }

    /**
     * @return float
     */
    public function getStepPriceDecimal()
    {
        return $this->stepPrice / 100;
    }
}