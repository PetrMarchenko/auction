<?php


namespace Admin\Controller;


use Admin\Model\TypeAdminModel;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class TypeAdminControllerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get('doctrine.entitymanager.orm_default');
        $typeAdminModel = $container->get(TypeAdminModel::class);

        return new TypeAdminController(
            $em,
            $typeAdminModel
        );
    }

}