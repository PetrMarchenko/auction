<?php


namespace Admin\Model;


use Goods\Entity\TypeGoods;

class TypeAdminModel
{

    const MESSAGE_SUCCESS_EDIT_TYPE = 'Data Type updated';

    /**
     * @var object EntityManager
     */
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * @return mixed
     */
    public function fetchAll() {
        $result = $this->em->getRepository(TypeGoods::class)
            ->findAll();
        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTypeById($id)
    {
        $result = $this->em->getRepository(TypeGoods::class)
            ->find($id);
        return $result;
    }

    /**
     * @param $types object
     * @return bool
     */
    public function update($types)
    {
        $typesDb = $this->em->getRepository(TypeGoods::class)
            ->find($types->getId());
        if (!$typesDb) {
            return false;
        }

        $typesDb->setOptions($types->getArrayCopy());
        $this->em->flush();

        return true;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function save($type)
    {
        $this->em->persist($type);
        $this->em->flush();
        return $type->getId();
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteById($id) {
        $type =  $this->getTypeById($id);
        if (!$type) {
            return false;
        }
        $this->em->remove($type);
        $this->em->flush();
        return true;
    }

}