<?php


namespace Admin\Form;


use Admin\Filter\UserInputFilter;
use Users\Form\RegistrationForm;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Hidden;

class UserForm extends RegistrationForm
{
    public function __construct()
    {
        parent::__construct();

        $this->add([
            'name' => 'id',
            'type' => Hidden::class,
        ]);

        $this->add([
            'type' => Checkbox::class,
            'name' => 'state',
            'options' => [
                'label' => 'State',
                'use_hidden_element' => true,
                'checked_value' => true,
                'unchecked_value' => false
            ],
        ]);

        $this->setInputFilter(new UserInputFilter());
    }

}