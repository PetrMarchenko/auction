<?php


namespace Admin\Form;

use Zend\Form\Element\Hidden;
use Zend\Form\Form;

class ConfirmForm extends Form
{
    public function __construct()
    {
        parent::__construct();

        $this->setAttribute('method', 'post');

        $this->add([
            'name' => 'id',
            'type' => Hidden::class,
        ]);

        $this->add([
            'name' => 'submitConfirm',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Delete',
                'class' => 'btn btn-warning btn-margin'
            ],
        ]);

    }

}